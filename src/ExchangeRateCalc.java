import java.util.Scanner;

/**
 * 
 * Very, very simple and naive console application to use ExchangeRates library
 * class
 * 
 * @author Libor Nekula 06/25/2017
 *
 */
public class ExchangeRateCalc {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ExchangeRates er = ExchangeRates.getInstance();
		Scanner input = new Scanner(System.in);

		while (true) {

			String from, to;
			float amount, result;

			System.out.println("Please enter currency code to convert FROM:");
			from = input.nextLine();
			System.out.println("Please enter currency code to convert TO:");
			to = input.nextLine();
			System.out.println("Please enter amount to convert:");
			amount = Float.parseFloat(input.nextLine());

			result = er.calculate(from, to, amount);

			// This should be improved to abs(1 - result) < delta
			if (result == -1.0) {

				System.out.println("Unsupported currency entered");
			} else {

				System.out.println(amount + " " + from + " is " + result + " " + to);
			}

			do {

				System.out.println("Do you want to continue? Y/N");
				from = input.nextLine();

			} while (!((from.equals("Y")) || (from.equals("N"))));

			if (from.equals("N")) {
				input.close();
				System.out.println("Thank you for using exchange rate calculator");
				return;
			}
		}
	}
}
