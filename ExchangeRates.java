/**
 * @author Beggycev
 *
 */

import java.io.InputStream;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ExchangeRates {

	private static ExchangeRates instance;
	private static Map<String, Float> ratesTable;

	/**
	 * constructor
	 */
	private ExchangeRates() {

		ratesTable = getValues();
	}

	/**
	 * @return instance of Exchange rates calls
	 */
	public static ExchangeRates getInstance() {

		if (instance == null) {

			instance = new ExchangeRates();
		}

		return instance;
	}

	/**
	 * Method checks db for existence of rate values if no values are present,
	 * they are synced from web
	 * 
	 * @return Map<String, Float> currency code - rate
	 */
	public Map<String, Float> getValues() {

		// init variables
		Connection c = getDBConn();
		Map<String, Float> result = null;

		if (c != null) {

			try {

				// Run the query
				Statement st = c.createStatement();

				String sql = "SELECT currency, rate FROM exchange_rates";
				ResultSet rs = st.executeQuery(sql);

				// if nothing found
				if (!rs.next()) {

					result = synchronize();

				} else {

					// Store results in HashMap
					result = new HashMap<String, Float>();

					while (rs.next()) {

						String currency = rs.getString("currency");
						Float rate = rs.getFloat("rate");

						result.put(currency, rate);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return result;
	}

	/**
	 * Download data from web, call parseXML to have HashMap created and save to
	 * DB
	 * 
	 * @return Map<String, Float> currency x rate
	 */
	private Map<String, Float> synchronize() {

		String target = getTarget();
		String xpathExp = getXPathExpression();
		Map<String, Float> data = null;

		// try to download xml doc from target web

		try {
			URL url = new URL(target);
			InputStream is = url.openStream();

			data = parseXML(is, xpathExp);

			// if any result was found store into db
			if (!data.isEmpty()) {

				Connection c = getDBConn();
				storeInDB(c, data);

			}

		} catch (Exception e) {

			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return data;
	}

	/**
	 * @param instream
	 *            - input stream representing source XML
	 * @param xpathExp
	 *            - XPath expression for parsing XML
	 * @return resulting Map currency x rate
	 */
	private Map<String, Float> parseXML(InputStream instream, String xpathExp) {

		Map<String, Float> resultMap = null;

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(instream);

			doc.getDocumentElement().normalize();
			XPath xPath = XPathFactory.newInstance().newXPath();

			String expression = xpathExp;
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);

			resultMap = new HashMap<String, Float>();

			for (int i = 0; i < nodeList.getLength(); i++) {

				Node nNode = nodeList.item(i);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println("\nCurrent Currency :" + eElement.getAttribute("currency"));
					System.out.println("\nCurrent Rate :" + eElement.getAttribute("rate"));
					String currency = eElement.getAttribute("currency");
					Float rate = Float.parseFloat(eElement.getAttribute("rate"));

					resultMap.put(currency, rate);
				}

			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return resultMap;
	}

	/**
	 * @param c
	 *            - DB connection
	 * @param data
	 *            - Map of currency x rate items to store
	 */
	private void storeInDB(Connection c, Map<String, Float> data) {

		Set<String> keyset = data.keySet();

		try {
			// Disabling auto commit to prevent commit after each insert
			c.setAutoCommit(false);
			Statement st = c.createStatement();

			// for each result
			for (String key : keyset) {

				// insert
				float rate = data.get(key);
				String sql = "INSERT INTO exchange_rates (currency, rate) VALUES(\'" + key + "\', " + rate + ")";
				System.out.println(sql);
				st.executeUpdate(sql);
			}

			// commit and close connection
			c.commit();
			c.close();

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	/**
	 * for case of future extension - can parse from file, etc...
	 * 
	 * @return String with XPath expression
	 */
	private String getXPathExpression() {

		String expression = "/RateExport/Day/Rate";
		return expression;
	}

	/**
	 * for case of future extension
	 * 
	 * @return string with tharget URL
	 */
	private String getTarget() {

		String target = "http://wizard.devsoft.cz/ecb.xml";
		return target;
	}

	/**
	 * @return DB connection
	 */
	private Connection getDBConn() {

		Connection connection = null;

		try {

			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/devsoft_task", "postgres",
					"postgres");

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		return connection;

	}

	/**
	 * @param from
	 *            - source currency code
	 * @param to
	 *            - target currency code
	 * @param amount
	 *            - amount to convert
	 * @return - result of conversion
	 */
	public float calculate(String from, String to, float amount) {

		// if same currencies, no calculation needed

		if (from.equals(to)) {
			return amount;
		}

		// check if currencies are euros, if not check their existence in ER
		// table

		else {

			Float fromRate = null;
			Float toRate = null;
			double delta = 0.00001;

			// EUR is "default currency with ER 1"
			if (from.equals("EUR")) {

				fromRate = new Float(1);

			} else if (to.equals("EUR")) {

				toRate = new Float(1);
			}

			// Get rate where currency is not EUR
			if (fromRate == null) {

				fromRate = ratesTable.get(from);
			}

			if (toRate == null) {

				toRate = ratesTable.get(to);
			}

			// If unsupported currency is entered
			if ((fromRate == null) || (toRate == null)) {

				return -1;
			}

			// return result
			else {

				return (amount / fromRate) * toRate;
			}
		}
	}
}
